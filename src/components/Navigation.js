import React from "react"
import { Navbar, Container, Nav } from "react-bootstrap"
import { useAuth } from "../contexts/AuthContext"
import { useHistory } from "react-router-dom"

export default function Navigation() {
    const { currentUser, logout } = useAuth()
    const history = useHistory()

    async function handleClickUserLogOut() {
        await logout()
        history.push("/login")
        
    }

    return (
        <>
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Container>
                    <Navbar.Brand href="/">Doctor Case Review</Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="ml-auto">
                            <Nav.Link href="/profile" className="ml-auto">Login with as {currentUser.displayName}</Nav.Link>
                            <div style={{width: "1px", backgroundColor: "white", marginTop: "12px", marginRight: "5px", marginLeft: "5px", height: "1em", display: "inline-block"}}></div>
                            <Nav.Link onClick={handleClickUserLogOut}>Log Out</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </>
    );
}