import React from "react"
import { AuthProvider } from "../contexts/AuthContext"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import Dashboard from "./Dashboard"
import Login from "./Login"
import Home from "./Home"
import PrivateRoute from "./PrivateRoute"
import Profile from "./Profile"
import UpdateProfile from "./UpdateProfile"

function App() {
  return (
    <div className="App">
      <Router>
        <AuthProvider>
          <Switch>
            <PrivateRoute exact path="/" component={Dashboard} />
            <PrivateRoute path="/profile" component={Profile} />
            <PrivateRoute path="/update-profile" component={UpdateProfile} />
            <Route path="/login" component={Login} />
            <Route path="/health" component={Home} />
          </Switch>
        </AuthProvider>
      </Router>
    </div>
  )
}

export default App
