import React, { useState, useEffect } from "react"
import { Row, Button, Container, Form, Col, Alert, Spinner } from "react-bootstrap"
import { useAuth } from "../contexts/AuthContext"
import Navigation from "./Navigation"

export default function Dashboard() {
  const [patientCase, setPatientCase] = useState({});
  const [conditions, setConditions] = useState([]);
  const [showLoading, setShowLoading] = useState(true);
  const [showForm, setShowForm] = useState(false);
  const [showAlert, setShowAlert] = useState(false);

  const [condition, setCondition] = useState(null);
  const [error, setError] = useState("");
  const { accessToken } = useAuth();

  const apiUrl = "https://api.webdatapower.tk";

  function handleChange(event) { setCondition(event.target.value); }

  function handleSubmit(event) {
    if(condition === null){
      setError("Please select one condition");
    } else {
      setShowForm(false);
      setShowAlert(false);
      setShowLoading(true);
      setError("");
      updateCase(condition);
    }
    event.preventDefault();
  }

  function resetForm(){
    setConditions([]);
    setPatientCase({});
    setCondition(null);
  }

  function fetchCase() {
    setShowLoading(true);
    fetch(`${apiUrl}/case`, {
      "method": "GET",
      "headers": {
        "authorization": `Bearer ${accessToken}`,
        "content-type": "application/json",
        "accept": "application/json"
      }
    })
    .then(res => {
      if(res.ok){
        setShowAlert(false);
        setShowForm(true);
        return res.json();
      }
      setShowAlert(true);
      setShowForm(false);

      return null;
    })
    .then(response => {
      setPatientCase(response);
    })
    .catch(error => console.log(error));
  }

  function updateCase(condition){
    fetch(`${apiUrl}/case/${patientCase.id}`, {
      "method": "PATCH",
      "headers": {
        "authorization": `Bearer ${accessToken}`,
        "content-type": "application/json",
        "accept": "application/json"
      },
      "body": JSON.stringify({ labels: [condition] })
    })
    .then(res => res.json())
    .then(response => {
      resetForm();
      fetchConditions();
      fetchCase();
      setShowLoading(false);
    })
    .catch(error => console.log(error));
  }

  function fetchConditions() {
    fetch(`${apiUrl}/conditions`, {
      "method": "GET",
      "headers": {
        "authorization": `Bearer ${accessToken}`,
        "content-type": "application/json",
        "accept": "application/json"
      }
    })
    .then(res => res.json())
    .then(response => {
      setConditions(response);
    })
    .catch(error => console.log(error));
  }

  useEffect(() => {
    fetchCase();
    fetchConditions();

    setShowLoading(false);
  }, [])

  return (
    <>
      <div>
        <Navigation/>
        <br />
        <Container>
        { showLoading ? <div style={{display: 'flex',  justifyContent:'center', alignItems:'center', height: '70vh'}}>
          <Spinner animation="border" />
        </div> : null }
        
        { showAlert ? <Alert variant="success">
            <Alert.Heading>Hey, nice to see you</Alert.Heading>
            <p>
            You don't have any more cases to review.
            </p>
            <hr />
            <p className="mb-0">
            You are Done!
            </p>
          </Alert> : null }
          
          {error && <Alert variant="danger">{error}</Alert>}
          { showForm ? <Form onSubmit={handleSubmit}>
            <Row className="mb-3">
              <Form.Group as={Col} controlId="formGridCase">
                <Form.Label><b>Please Review This Case</b></Form.Label>
                <Form.Control
                  as="textarea"
                  placeholder=""
                  value={patientCase.ehr}
                  style={{ height: '500px' }}
                  disabled
                />
              </Form.Group>
              <Form.Group as={Col} controlId="formGridConditions">
                <Form.Label><b>Select Condition</b></Form.Label>
                <Form.Control as="select" multiple style={{ height: '200px' }} onChange={handleChange} >
                  {conditions.map((c) => (
                     <option value={c.code} key={c.id}>
                      {c.title} ({c.code})
                    </option>
                  ))}
                </Form.Control>
              </Form.Group>
            </Row>
            <Row>
              <Button variant="primary" type="submit">
                Next Case
              </Button>
            </Row>
          </Form> : null }
        </Container>
      </div>
    </>
  )
}
