# Doctor case review UI

## TLDR

The demo aplication can be found in here:<br>
https://doctor.webdatapower.tk/

<b>User:</b> doctor@admin.com<br>
<b>Password:</b> P@ssw0rd

|  |  |   |
|-----------------|:-------------|:---------------:|
| ![login](images/login.jpg) | ![review](images/review.jpg)  | ![review](images/case_error.jpg)|
| ![review](images/case_empty.jpg)     | ![review](images/profile.jpg)          |       |

## Description

In this repository you can find the frontend code for the application.

All requirements have been added to the gitlab issues board which can be viewed [here](https://gitlab.com/groups/ruben-challenge/-/boards).

To protect the app, Firebase Authentication was used so that only a user with a valid JWT can access the app resources.

![firebase](images/firebase.png)

This application was also deployed in a private kubernetes custer installed in the Google Cloud Platform and integrated with [Gitlab pipelines](https://gitlab.com/ruben-challenge/frontend/-/pipelines). All of this has been done as a bonus so that it can be consulted and used for testing.

![gitlab](images/gitlab.jpg)

![gke](images/gke.jpg)

## Implemented Features
* Authentication based on JWT token using Firebase auth;
* Login page to authenticate users in firebase;
* Profile page to get user information from firebase;
* Health page to get the application health for kubernetes deployment;
* Home page to get all the conditions existing in the database, the available case and get next case;

## Installation

```bash
$ npm install
```

## Configuration

Change inside the `.env` file the connection string for mongodb.

## Running the app

```bash
# development
$ npm run start
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Using docker-compose

```bash
# start containers
$ docker-compose up -d
```
