FROM node:12.13-alpine AS builder

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . ./

RUN npm run build

FROM nginx:stable-alpine AS production

ENV NODE_ENV=production

COPY --from=builder /usr/src/app/build/ /usr/share/nginx/html

COPY ./config/nginx/nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 5000

CMD ["/bin/sh", "-c", "exec nginx -g 'daemon off;'"]
